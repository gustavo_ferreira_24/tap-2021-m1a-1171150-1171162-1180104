package domain.schedule

import domain.domain.PhysicalResource
import org.scalacheck.Gen

object PhysicalResourcesGenerator:

  def genPhysicalResource(phyId: String): Gen[PhysicalResource] =
    for
      id <- SimpleTypesGenerator.genPhysicalResourceId(phyId)
      prType <- SimpleTypesGenerator.genPhysicalResourceType(phyId)
    yield PhysicalResource(id, prType)

  //------1
  def genPhysicalResources: Gen[List[PhysicalResource]] = UtilsGenerator.genListOfUniqueDomain(10, 15, genPhysicalResource)
