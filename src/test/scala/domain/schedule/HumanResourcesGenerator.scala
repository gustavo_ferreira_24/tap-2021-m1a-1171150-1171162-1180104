package domain.schedule

import domain.domain.HumanResource
import domain.domain.SimpleTypes.*
import org.scalacheck.Gen

object HumanResourcesGenerator:

  def genHumanResource(lprt: List[PhysicalResourceType])(humId: String): Gen[HumanResource] =
    for
      id <- SimpleTypesGenerator.genHumanResourceId(humId)
      name <- SimpleTypesGenerator.genNonEmptyString
    yield HumanResource(id, name, lprt)

  def genHumanResources(lprt: List[PhysicalResourceType]): Gen[List[HumanResource]] = UtilsGenerator.genListOfUniqueDomain(lprt.size, lprt.size, genHumanResource(lprt))
