package domain.schedule

import scala.language.adhocExtensions
import domain.DomainError.*
import domain.Result
import domain.domain.*
import domain.domain.SimpleTypes.*
import org.scalacheck.*
import org.scalacheck.Prop.*
import domain.DomainError

object ScheduleMS02Properties extends Properties("Schedule MS02 ") :

  
  property("The complete schedule must schedule all the tasks of all the products needed") =
    forAll(ProductionGenerator.genProduction)(
      production =>
        val scheduledDomain = ScheduleMS01.extractProductionToScheduleDomain(production)
        val totalTasks_In = production.orders.map(o => o.product).map(p => p.tasksList).flatten
        scheduledDomain.fold(_ => false, sched =>
          val totalTasksSchedule_Out = sched.taskScheduleList.map(tsl => tsl.task)
          totalTasks_In.forall(t => totalTasksSchedule_Out.contains(t))
        )
    )

  property("The same physical resource cannot be used more than once by the same task") =
    forAll(ProductionGenerator.genProduction)(
      production =>
        val scheduledDomain = ScheduleMS01.extractProductionToScheduleDomain(production)
        scheduledDomain.fold(_ => false, sched =>
          sched.taskScheduleList.forall(ts => ts.physicalResources.size == ts.physicalResources.distinctBy(_.id).size)
        )
  )

  property("The same human resource cannot be used more than once by the same task") =
    forAll(ProductionGenerator.genProduction)(
      production =>
        val scheduledDomain = ScheduleMS01.extractProductionToScheduleDomain(production)
          scheduledDomain.fold(_ => false, sched =>
      sched.taskScheduleList.forall(ts => ts.humanResources.size == ts.humanResources.distinctBy(_.id).size)
    )
  )

  property("Production Orders are processed in the order stated by production") =
    forAll(ProductionGenerator.genProduction)(
      production =>
        val productionOrders = production.orders
        val scheduledDomain = ScheduleMS01.extractProductionToScheduleDomain(production)
        scheduledDomain.fold(_ => false, sched =>
          val scheduleOrders = sched.taskScheduleList.map(ts => ts.order).distinctBy(_.orderId)
          productionOrders == scheduleOrders
        )
    )

  // Only applicable to MS01
  property("The start time of a task must be the same as the end time of the task before it") =
    forAll(ProductionGenerator.genProduction)(
      production =>
        val scheduledDomain = ScheduleMS01.extractProductionToScheduleDomain(production)
        scheduledDomain.fold(_ => false, sched =>
          sched.taskScheduleList.foldLeft(0,true){ case (tuple, ts) =>
            if(tuple._2 == false) {
              (ts.finalTime.to,false)
            }
            if(ts.startingTime.to == 0){
              (ts.finalTime.to,true)
            } else {
              if(ts.startingTime.to == tuple._1){
                (ts.finalTime.to,true)
              }else{
                (ts.finalTime.to,false)
              }
            }
          }._2
        )
    )



  property("The same human resource cannot be used at the same time by two tasks") =
    forAll(ProductionGenerator.genProduction)(
      production =>
        val scheduledDomain = ScheduleMS01.extractProductionToScheduleDomain(production)
        scheduledDomain.fold(_ => false, sched => 
        sched.taskScheduleList.forall( ts =>
          {
            val filteredTasksSchedules:List[TaskSchedule] =
            sched.taskScheduleList.filter( tsl =>
              tsl.startingTime < ts.finalTime && tsl.finalTime > ts.startingTime
              && tsl.order != ts.order && tsl.productNumber != ts.productNumber && tsl.task != ts.task)
            
              val filteredHResources = filteredTasksSchedules.flatMap(ft => ft.humanResources)
            
              ts.humanResources.forall(hr => !filteredHResources.contains(hr))
          } )
      )
    )
  
  property("The same physical resource cannot be used at the same time by two tasks") =
    forAll(ProductionGenerator.genProduction)(
      production =>
        val scheduledDomain = ScheduleMS01.extractProductionToScheduleDomain(production)
        scheduledDomain.fold(_ => false, sched => 
        sched.taskScheduleList.forall( ts =>
          {
            val filteredTasksSchedules:List[TaskSchedule] =
            sched.taskScheduleList.filter( tsl =>
              tsl.startingTime < ts.finalTime && tsl.finalTime > ts.startingTime
              && tsl.order != ts.order && tsl.productNumber != ts.productNumber && tsl.task != ts.task)
            
              val filteredPResources = filteredTasksSchedules.flatMap(ft => ft.physicalResources)
            
              ts.physicalResources.forall(phyR => !filteredPResources.contains(phyR))
    
          } )
      )
    )

  property("For all orders in scheduled tasks, the max product number of the respective poduct must be equal " +
    "to the quantity of the same product in the correspondent production order") =
    forAll(ProductionGenerator.genProduction)(
      production =>
        val scheduledDomain = ScheduleMS01.extractProductionToScheduleDomain(production)
        scheduledDomain.fold(_ => false,
          sched => sched.taskScheduleList.map(ts => ts.order).forall( order =>
          val maxProductNumber=
            sched.taskScheduleList.filter(t => t.order == order).maxBy(_.productNumber).productNumber
      
          val quantity: Int =
            production.orders.find(o=> o == order) match {
              case Some(o) => o.quantity.to
              case _ => 0
            }
          maxProductNumber == quantity
        )
      )
    )
  
  property("The number of physical resources used in a task should always be equal to the number of human resources" +
    " of the same task") =
    forAll(ProductionGenerator.genProduction)(
      production =>
        val scheduledDomain = ScheduleMS01.extractProductionToScheduleDomain(production)
        scheduledDomain.fold(_ => false,
          sched => sched.taskScheduleList.forall(ts => ts.humanResources.size == ts.physicalResources.size)))

  property("The human resources allocated to a task should handle at least one of the physical resources" +
    " allocated to the same task") =
    forAll(ProductionGenerator.genProduction)(
      production =>
        val scheduledDomain = ScheduleMS01.extractProductionToScheduleDomain(production)
        scheduledDomain.fold(_ => false,
          sched => sched.taskScheduleList.forall(ts => ts.humanResources.forall(
            hr => hr.handles.exists( prt => ts.physicalResources.map(pr => pr.phyResourceType).contains(prt))))))

  property("All physical resources allocated to a task are required for the execution of the same task." ) =
    forAll(ProductionGenerator.genProduction)(
      production =>
        val scheduledDomain = ScheduleMS01.extractProductionToScheduleDomain(production)
        scheduledDomain.fold(_ => false,
          sched => sched.taskScheduleList.forall(ts => ts.physicalResources.forall(
            pr => ts.task.physicalResourceTypes.contains(pr.phyResourceType)))) )


  property("The human resources allocated to a task should be able to performe the same task") =
    forAll(ProductionGenerator.genProduction)(
      production =>
        val scheduledDomain = ScheduleMS01.extractProductionToScheduleDomain(production)
        scheduledDomain.fold(_ => false,
          sched => sched.taskScheduleList.forall(ts => ts.humanResources.forall(
            hr => hr.handles.exists( prt => ts.task.physicalResourceTypes.contains(prt))))) )
  

  property("The end time of a task should always be greater then the begin time") =
    forAll(ProductionGenerator.genProduction)(
      production =>
        ScheduleMS01.extractProductionToScheduleDomain(production).fold(_ => false, sched =>
          sched.taskScheduleList.forall(taskSchedule => taskSchedule.startingTime < taskSchedule.finalTime)
        )
    )

  property("The complete schedule must contain all the orders requested") =
    forAll(ProductionGenerator.genProduction)(production =>
      ScheduleMS01.extractProductionToScheduleDomain(production).fold(
        _ => false,
        schedule => schedule.taskScheduleList.map(taskSchedule => taskSchedule.order).distinct == production.orders
      )
    )

  property("If a task uses an unavailable physical resource it should result in the respective domain error") =
    forAll(ProductionGenerator.genProductionWithInvalidPhysicalResources)(production =>
      ScheduleMS01.extractProductionToScheduleDomain(production) match {
        case Left(ResourceUnavailable(_,_)) => true
        case _  => false
      }
    )

  property("If a task uses an unavailable human resource it should result in the respective domain error") =
    forAll(ProductionGenerator.genProductionWithInvalidHumanResources)(production =>
      ScheduleMS01.extractProductionToScheduleDomain(production) match {
        case Left(ResourceUnavailable(_,_)) => true
        case _  => false
      }
    )
