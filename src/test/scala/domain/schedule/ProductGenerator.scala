package domain.schedule

import domain.domain.Product
import domain.domain.Task
import org.scalacheck.Gen

object ProductGenerator:

  def genProduct(lt: List[Task])(id: String): Gen[Product] =
    for
      id <- SimpleTypesGenerator.genProductId(id)
      name <- SimpleTypesGenerator.genNonEmptyString
      n <- Gen.chooseNum(1, lt.size)
      tasks <- Gen.pick(n, lt)
    yield Product(id, name, tasks.toList)

  def genProducts(lt: List[Task]): Gen[List[Product]] = UtilsGenerator.genListOfUniqueDomain(4, 6, genProduct(lt))