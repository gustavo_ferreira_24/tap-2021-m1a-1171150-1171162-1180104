package domain.schedule

import domain.domain.Order
import domain.domain.Product
import org.scalacheck.Gen

object OrderGenerator:

  def genOrder(lp: List[Product])(id: String): Gen[Order] =
    for
      id <- SimpleTypesGenerator.genOrderId(id)
      product <- Gen.oneOf(lp)
      quantity <- SimpleTypesGenerator.genQuantity
    yield Order(id, product, quantity)

  def genOrders(lp: List[Product]): Gen[List[Order]] = UtilsGenerator.genListOfUniqueDomain(4, lp.size, genOrder(lp))
