package domain.schedule

import domain.domain.SimpleTypes.*
import org.scalacheck.Gen

object SimpleTypesGenerator:

  def genUniqueIds(min: Int, max: Int): Gen[List[String]] =
    for
      n <- Gen.choose(min, max)
      lIds = (1 to n).map(id => "%02d".format(id)).toList
    yield lIds

  def genHumanResourceId(id: String): Gen[HumanResourceId] = HumanResourceId.from("HRS_" + id).fold(_ => Gen.fail, id => Gen.const(id))

  def genPhysicalResourceId(id: String): Gen[PhysicalResourceId] = PhysicalResourceId.from("PRS_" + id).fold(_ => Gen.fail, id => Gen.const(id))

  def genTaskId(id: String): Gen[TaskId] = TaskId.from("TSK_" + id).fold(_ => Gen.fail, id => Gen.const(id))

  def genProductId(id: String): Gen[ProductId] = ProductId.from("PRD_" + id).fold(_ => Gen.fail, id => Gen.const(id))

  def genOrderId(id: String): Gen[OrderId] = OrderId.from("ORD_" + id).fold(_ => Gen.fail, id => Gen.const(id))

  def genPhysicalResourceType(id: String): Gen[PhysicalResourceType] = PhysicalResourceType.from("PRST_" + id).fold(_ => Gen.fail, phyType => Gen.const(phyType))

  def genNonEmptyString: Gen[NonEmptyString] =
    for
      n <- Gen.choose(5, 20)
      lc <- Gen.listOfN(n, Gen.alphaChar)
      nes <- NonEmptyString.from(lc.mkString).fold(_ => Gen.fail, s => Gen.const(s))
    yield nes

  def genQuantity: Gen[Quantity] =
    for
      n <- Gen.choose(1, 20)
      quantity <- Quantity.from(n.toString).fold(_ => Gen.fail, quantity => Gen.const(quantity))
    yield quantity

  def genTaskDuration: Gen[TaskDuration] =
    for
      n <- Gen.choose(1, 200)
      taskDuration <- TaskDuration.from(n.toString).fold(_ => Gen.fail, taskDuration => Gen.const(taskDuration))
    yield taskDuration
