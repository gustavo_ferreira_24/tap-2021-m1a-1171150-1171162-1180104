package domain.schedule

import domain.domain.Task
import domain.domain.SimpleTypes.*
import org.scalacheck.Gen

object TaskGenerator:

  //pegar nos tipos dos resuros fisicos e gerar tarefas
  //-------3
  def genTask(lprt: List[PhysicalResourceType])(id: String): Gen[Task] =
    for {
      id <- SimpleTypesGenerator.genTaskId(id)
      duration <- SimpleTypesGenerator.genTaskDuration
      n <- Gen.chooseNum(1, lprt.size)
      prstList <- Gen.pick(n, lprt)
    } yield Task(id, duration, prstList.toList)

  def genTasks(lprt: List[PhysicalResourceType]): Gen[List[Task]] = UtilsGenerator.genListOfUniqueDomain(6, 8, genTask(lprt))
