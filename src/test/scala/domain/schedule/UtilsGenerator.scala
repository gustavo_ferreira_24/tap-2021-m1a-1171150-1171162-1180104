package domain.schedule

import org.scalacheck.Gen

object UtilsGenerator:

  def genListOfUniqueDomain[A](minEntities: Int, maxEntities: Int, f: String => Gen[A]) =
    for
      idsList <- SimpleTypesGenerator.genUniqueIds(minEntities, maxEntities)
      listOfEntities = idsList.map(f)
      finalListOfEntities <- Gen.sequence[List[A], A](listOfEntities)
    yield finalListOfEntities
