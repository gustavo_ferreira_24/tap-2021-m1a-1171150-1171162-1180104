package domain.schedule

import domain.domain.*
import domain.domain.SimpleTypes.*
import org.scalacheck.Gen

object ProductionGenerator:

  // TODO: Possivelmente melhorar
  def genProductionWithInvalidPhysicalResources: Gen[Production] =
    for
      lpr <- PhysicalResourcesGenerator.genPhysicalResources
      lprt = extractTypesFromPhysicalResources(lpr)
      hrs <- HumanResourcesGenerator.genHumanResources(lprt)
      tsks <- TaskGenerator.genTasks(lprt)
      prds <- ProductGenerator.genProducts(tsks)
      ords <- OrderGenerator.genOrders(prds)
    yield Production(List(), tsks, hrs, prds, ords)

  // TODO: Possivelmente melhorar
  def genProductionWithInvalidHumanResources: Gen[Production] =
    for
      lpr <- PhysicalResourcesGenerator.genPhysicalResources
      lprt = extractTypesFromPhysicalResources(lpr)
      hrs <- HumanResourcesGenerator.genHumanResources(lprt)
      tsks <- TaskGenerator.genTasks(lprt)
      prds <- ProductGenerator.genProducts(tsks)
      ords <- OrderGenerator.genOrders(prds)
    yield Production(lpr, tsks, List(), prds, ords)
  
  def genProduction: Gen[Production] =
    for
      lpr <- PhysicalResourcesGenerator.genPhysicalResources
      lprt = extractTypesFromPhysicalResources(lpr)
      hrs <- HumanResourcesGenerator.genHumanResources(lprt)
      tsks <- TaskGenerator.genTasks(lprt)
      prds <- ProductGenerator.genProducts(tsks)
      ords <- OrderGenerator.genOrders(prds)
    yield Production(lpr, tsks, hrs, prds, ords)

  // Pegar no recuros físicos e retirar-lhes os tipos de recursos físicos
  private def extractTypesFromPhysicalResources(lpr: List[PhysicalResource]): List[PhysicalResourceType] = lpr.map(phyResource => phyResource.phyResourceType)
