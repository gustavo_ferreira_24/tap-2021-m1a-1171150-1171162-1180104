package domain.domain

import scala.language.adhocExtensions
import org.scalatest.funsuite.AnyFunSuite

import scala.xml.Node
import xml.XML.*
import domain.DomainError.*

class ProductionTest extends AnyFunSuite:
  
  
  test("Should convert an invalid Production in XML format to the respective XMLError "){

    val productionXML1:Node = 
      <Production>
      <Tasks>
        <Task id="TSK_1" time="100">
          <PhysicalResource type="PRST 1"/>
          <PhysicalResource type="PRST 4"/>
        </Task>
        <Task id="TSK_2" time="80">
          <PhysicalResource type="PRST 2"/>
          <PhysicalResource type="PRST 5"/>
        </Task>
      </Tasks>
      <HumanResources>
        <Human id="HRS_1" name="Antonio">
          <Handles type="PRST 1"/>
          <Handles type="PRST 2"/>
        </Human>
        <Human id="HRS_2" name="Maria">
          <Handles type="PRST 3"/>
          <Handles type="PRST 4"/>
          <Handles type="PRST 5"/>
        </Human>
      </HumanResources>
      <Products>
        <Product id="PRD_1" name="Product 1">
          <Process tskref="TSK_1"/>
          <Process tskref="TSK_2"/>
        </Product>
        <Product id="PRD_2" name="Product 2">
          <Process tskref="TSK_2"/>
        </Product>
      </Products>
      <Orders>
        <Order id="ORD_1" prdref="PRD_1" quantity="1"/>
        <Order id="ORD_2" prdref="PRD_2" quantity="2"/>
      </Orders>
    </Production>

    val productionXML2:Node =
      <Production>
        <PhysicalResources>
          <Physical id="PRS_1" type="PRST 1"/>
          <Physical id="PRS_2" type="PRST 2"/>
          <Physical id="PRS_3" type="PRST 3"/>
          <Physical id="PRS_4" type="PRST 4"/>
          <Physical id="PRS_5" type="PRST 5"/>
        </PhysicalResources>
        <HumanResources>
          <Human id="HRS_1" name="Antonio">
            <Handles type="PRST 1"/>
            <Handles type="PRST 2"/>
          </Human>
          <Human id="HRS_2" name="Maria">
            <Handles type="PRST 3"/>
            <Handles type="PRST 4"/>
            <Handles type="PRST 5"/>
          </Human>
        </HumanResources>
        <Products>
          <Product id="PRD_1" name="Product 1">
            <Process tskref="TSK_1"/>
            <Process tskref="TSK_2"/>
          </Product>
          <Product id="PRD_2" name="Product 2">
            <Process tskref="TSK_2"/>
          </Product>
        </Products>
        <Orders>
          <Order id="ORD_1" prdref="PRD_1" quantity="1"/>
          <Order id="ORD_2" prdref="PRD_2" quantity="2"/>
        </Orders>
      </Production>


    val productionXML3:Node =
      <Production>
        <PhysicalResources>
          <Physical id="PRS_1" type="PRST 1"/>
          <Physical id="PRS_2" type="PRST 2"/>
          <Physical id="PRS_3" type="PRST 3"/>
          <Physical id="PRS_4" type="PRST 4"/>
          <Physical id="PRS_5" type="PRST 5"/>
        </PhysicalResources>
        <Tasks>
          <Task id="TSK_1" time="100">
            <PhysicalResource type="PRST 1"/>
            <PhysicalResource type="PRST 4"/>
          </Task>
          <Task id="TSK_2" time="80">
            <PhysicalResource type="PRST 2"/>
            <PhysicalResource type="PRST 5"/>
          </Task>
        </Tasks>
        <Products>
          <Product id="PRD_1" name="Product 1">
            <Process tskref="TSK_1"/>
            <Process tskref="TSK_2"/>
          </Product>
          <Product id="PRD_2" name="Product 2">
            <Process tskref="TSK_2"/>
          </Product>
        </Products>
        <Orders>
          <Order id="ORD_1" prdref="PRD_1" quantity="1"/>
          <Order id="ORD_2" prdref="PRD_2" quantity="2"/>
        </Orders>
      </Production>

    val productionXML4:Node =
      <Production>
        <PhysicalResources>
          <Physical id="PRS_1" type="PRST 1"/>
          <Physical id="PRS_2" type="PRST 2"/>
          <Physical id="PRS_3" type="PRST 3"/>
          <Physical id="PRS_4" type="PRST 4"/>
          <Physical id="PRS_5" type="PRST 5"/>
        </PhysicalResources>
        <Tasks>
          <Task id="TSK_1" time="100">
            <PhysicalResource type="PRST 1"/>
            <PhysicalResource type="PRST 4"/>
          </Task>
          <Task id="TSK_2" time="80">
            <PhysicalResource type="PRST 2"/>
            <PhysicalResource type="PRST 5"/>
          </Task>
        </Tasks>
        <HumanResources>
          <Human id="HRS_1" name="Antonio">
            <Handles type="PRST 1"/>
            <Handles type="PRST 2"/>
          </Human>
          <Human id="HRS_2" name="Maria">
            <Handles type="PRST 3"/>
            <Handles type="PRST 4"/>
            <Handles type="PRST 5"/>
          </Human>
        </HumanResources>
        <Orders>
          <Order id="ORD_1" prdref="PRD_1" quantity="1"/>
          <Order id="ORD_2" prdref="PRD_2" quantity="2"/>
        </Orders>
      </Production>

    val productionXML5:Node =
      <Production>
        <PhysicalResources>
          <Physical id="PRS_1" type="PRST 1"/>
          <Physical id="PRS_2" type="PRST 2"/>
          <Physical id="PRS_3" type="PRST 3"/>
          <Physical id="PRS_4" type="PRST 4"/>
          <Physical id="PRS_5" type="PRST 5"/>
        </PhysicalResources>
        <Tasks>
          <Task id="TSK_1" time="100">
            <PhysicalResource type="PRST 1"/>
            <PhysicalResource type="PRST 4"/>
          </Task>
          <Task id="TSK_2" time="80">
            <PhysicalResource type="PRST 2"/>
            <PhysicalResource type="PRST 5"/>
          </Task>
        </Tasks>
        <HumanResources>
          <Human id="HRS_1" name="Antonio">
            <Handles type="PRST 1"/>
            <Handles type="PRST 2"/>
          </Human>
          <Human id="HRS_2" name="Maria">
            <Handles type="PRST 3"/>
            <Handles type="PRST 4"/>
            <Handles type="PRST 5"/>
          </Human>
        </HumanResources>
        <Products>
          <Product id="PRD_1" name="Product 1">
            <Process tskref="TSK_1"/>
            <Process tskref="TSK_2"/>
          </Product>
          <Product id="PRD_2" name="Product 2">
            <Process tskref="TSK_2"/>
          </Product>
        </Products>
      </Production>
    
    assert(Production.from(productionXML1).
      fold(de=>de==XMLError("Node PhysicalResources is empty/undefined in Production"),_=>false))
    assert(Production.from(productionXML2).
      fold(de=>de==XMLError("Node Tasks is empty/undefined in Production"),_=>false))
    assert(Production.from(productionXML3).
      fold(de=>de==XMLError("Node HumanResources is empty/undefined in Production"),_=>false))
    assert(Production.from(productionXML4).
      fold(de=>de==XMLError("Node Products is empty/undefined in Production"),_=>false))
    assert(Production.from(productionXML5).
      fold(de=>de==XMLError("Node Orders is empty/undefined in Production"),_=>false))
  }

