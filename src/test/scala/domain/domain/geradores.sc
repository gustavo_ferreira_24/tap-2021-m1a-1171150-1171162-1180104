import scala.xml.Node
import scala.xml.Elem
import xml.XML.*
import domain.domain.TaskMock.*
import domain.domain.OrderMock.*
import domain.domain.ProductMock.*
import domain.DomainError.*
import domain.Result
import domain.domain.*
import domain.domain.SimpleTypes.*
import org.scalacheck.{Gen, Prop}
import org.scalacheck.Prop.forAll

def genStringPattern(s: String):Gen[String]=
  for{
    n<- Gen.choose(0,9)
    ld<-Gen.listOfN(n,Gen.chooseNum(0,9))
    lc<-Gen.listOfN(n,Gen.alphaChar)
} yield (s+ld.mkString)

genStringPattern("PRD_").sample
genStringPattern("PRD_").sample
genStringPattern("PRD_").sample


val x=Gen.oneOf("A","Z")
x.sample

val n=Gen.choose(0,9)
n.sample

val nn=Gen.chooseNum(0,9)

def genProductId: Gen[ProductId]=
  for{
    s<-genStringPattern("PRD_")
    pid<- ProductId.from(s).fold(_=>Gen.fail, id=>Gen.const(id))
  } yield pid

genProductId.sample
genProductId.sample
genProductId.sample

def genOrderId: Gen[OrderId]=
  for{
    s<-genStringPattern("ORD_")
    oid<- OrderId.from(s).fold(_=>Gen.fail, id=>Gen.const(id))
  } yield oid

genOrderId.sample
genOrderId.sample
genOrderId.sample

def genNonEmptyString: Gen[NonEmptyString]=
  for{
    n<-Gen.choose(5,15)
    lc<-Gen.listOfN(n,Gen.alphaChar)
    nes<-NonEmptyString.from(lc.mkString).fold(_=>Gen.fail,s=>Gen.const(s))
  } yield nes

genNonEmptyString.sample
genNonEmptyString.sample
genNonEmptyString.sample

def genQuantity: Gen[Quantity]=
  for{
    n<-Gen.choose(1,100)
    q<- Quantity.from(n.toString).fold(_=>Gen.fail, i=>Gen.const(i))
  } yield q

genQuantity.sample
genQuantity.sample
genQuantity.sample

def genTaskId: Gen[TaskId]=
  for{
    s<-genStringPattern("TSK_")
    t<- TaskId.from(s).fold(_=>Gen.fail, id=>Gen.const(id))
  } yield t

genTaskId.sample
genTaskId.sample
genTaskId.sample


def genDuration: Gen[TaskDuration]=
  for{
    n<-Gen.choose(1,300)
    d<- TaskDuration.from(n.toString).fold(_=>Gen.fail, i=>Gen.const(i))
  } yield d

genDuration.sample
genDuration.sample
genDuration.sample


def genPhysicalResourceType: Gen[PhysicalResourceType]=
  for{
    n<-Gen.choose(5,15)
    lc<-Gen.listOfN(n,Gen.alphaChar)
    t<-PhysicalResourceType.from("PRST_"+lc.mkString).fold(_=>Gen.fail,s=>Gen.const(s))
  } yield t

genPhysicalResourceType.sample
genPhysicalResourceType.sample
genPhysicalResourceType.sample

def genHumanResourceId: Gen[HumanResourceId]=
  for{
    s<-genStringPattern("HRS_")
    hrid<- HumanResourceId.from(s).fold(_=>Gen.fail, id=>Gen.const(id))
  } yield hrid

genHumanResourceId.sample
genHumanResourceId.sample
genHumanResourceId.sample

def genPhysicalResourceId: Gen[PhysicalResourceId]=
  for{
    s<-genStringPattern("PRS_")
    hrid<- PhysicalResourceId.from(s).fold(_=>Gen.fail, id=>Gen.const(id))
  } yield hrid

def genHumanResource: Gen[HumanResource]=
  for{

    id<-genHumanResourceId
    name<- genNonEmptyString
    n<- Gen.choose(1,5)
    handles<- Gen.listOfN(n,genPhysicalResourceType)
  } yield HumanResource(id,name,handles)

genHumanResource.sample
genHumanResource.sample
genHumanResource.sample

def genPhysicalResource: Gen[PhysicalResource]=
  for{
    id<-genPhysicalResourceId
    prType<- genPhysicalResourceType
  } yield PhysicalResource(id,prType)

genPhysicalResource.sample
genPhysicalResource.sample
genPhysicalResource.sample

def genTask: Gen[Task] =
  for{
    id<-genTaskId
    duration<-genDuration
    n<-Gen.choose(1,5)
    prstList<- Gen.listOfN(n,genPhysicalResourceType)
  } yield Task(id,duration,prstList)

genTask.sample
genTask.sample
genTask.sample

def genProduct: Gen[domain.domain.Product] =
  for{
    id<- genProductId
    name<- genNonEmptyString
    n <- Gen.choose(1,5)
    tasks<-Gen.listOfN(n, genTask)
  } yield Product(id,name,tasks)

genProduct.sample
genProduct.sample
genProduct.sample

def genOrder: Gen[Order]=
  for{
    id<- genOrderId
    product<- genProduct
    quantity <-genQuantity
  } yield Order(id,product,quantity)

genOrder.sample
genOrder.sample
genOrder.sample

def genProduction: Gen[Production]=
  for{
    pr<- genPhysicalResource
    t<- genTask
    hr<- genHumanResource
    p<- genProduct
    o<- genOrder
    n<- Gen.choose(1,5)
   // phrs<- Gen.nonEmptyContainerOf[Seq,PhysicalResource](pr)
    phrs<- Gen.listOfN(n,pr)
    tsks <- Gen.listOfN(n,t)
    hrs <- Gen.listOfN(n,hr)
    //prds <- Gen.nonEmptyContainerOf[Seq,domain.domain.Product](p)
    prds<-Gen.listOfN(n,p)
    ords<- Gen.listOfN(n,o)
  } yield Production(phrs,tsks,hrs,prds,ords)

genProduction.sample
genProduction.sample
genProduction.sample


def genNonEmpty= Gen.nonEmptyContainerOf[List, Char](Gen.alphaChar)
genNonEmpty.sample
genNonEmpty.sample
genNonEmpty.sample
