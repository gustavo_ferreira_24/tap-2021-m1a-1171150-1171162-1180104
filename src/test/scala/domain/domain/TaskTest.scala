package domain.domain

import domain.domain.SimpleTypes.*
import domain.DomainError.*
import domain.Result

import scala.xml.Node

import scala.language.adhocExtensions
import org.scalatest.funsuite.AnyFunSuite

class TaskTest extends AnyFunSuite :

  import domain.domain.TaskMock.*

  test("Should convert a valid Task in XML format to the internal domain object") {
    val taskXML1 = <Task id="TSK_1" time="100">
      <PhysicalResource type="PRST 1"/>
      <PhysicalResource type="PRST 4"/>
    </Task>
    val taskXML2 = <Task id="TSK_2" time="3000">
      <PhysicalResource type="PRST 4"/>
    </Task>


    assert(createMockTask(taskXML1).fold(de => false, result =>
      result.id.to == "TSK_1" && result.duration.to == 100 && result.physicalResourceTypes.equals(List(("PRST 1"), ("PRST 4")))))

    assert(createMockTask(taskXML2).fold(de => false, result =>
      result.id.to == "TSK_2" && result.duration.to == 3000 && result.physicalResourceTypes.equals(List(("PRST 4")))))
  }

  test("Should convert an invalid Task in XML format to the respective Domain Error") {
    val taskXML1 = <Task id="TK_1" time="100">
      <PhysicalResource type="PRST 1"/>
      <PhysicalResource type="PRST 4"/>
    </Task>
    val taskXML2 = <Task id="TSK-2" time="100">
      <PhysicalResource type="PRST 4"/>
    </Task>
    val taskXML3 = <Task id="TSK3" time="100">
      <PhysicalResource type="PRST 4"/>
    </Task>
    val taskXML4 = <Task id="TSK_3" time="0">
      <PhysicalResource type="PRST 4"/>
    </Task>
    val taskXML5 = <Task id="TSK_3" time="-1">
      <PhysicalResource type="PRST 4"/>
    </Task>
    val taskXML6 = <Task idd="TSK3" time="100">
      <PhysicalResource type="PRST 4"/>
    </Task>
    val taskXML7 = <Task id="TSK_3" timse="100">
      <PhysicalResource type="PRST 4"/>
    </Task>
    val taskXML8 = <Task id="TSK_5" time="100">
      <PhysicalResource type1="PRST 4"/>
    </Task>
    val taskXML9 = <Task id="TSK_5" time="100">
      <PhysicalResource type="PRST 2"/>
    </Task>
    val taskXML10 = <Task id="TSK_1" time="100">
      <PhysicalResource type="PRST 1"/>
      <PhysicalResource type="PRST 2"/>
    </Task>


    assert(createMockTask(taskXML1).fold(de => de == InvalidTaskId("TK_1"), _ => false))
    assert(createMockTask(taskXML2).fold(de => de == InvalidTaskId("TSK-2"), _ => false))
    assert(createMockTask(taskXML3).fold(de => de == InvalidTaskId("TSK3"), _ => false))
    assert(createMockTask(taskXML4).fold(de => de == InvalidTaskTime("0"), _ => false))
    assert(createMockTask(taskXML5).fold(de => de == InvalidTaskTime("-1"), _ => false))
    assert(createMockTask(taskXML6).fold(de => de == XMLError("Attribute id is empty/undefined in Task"), _ => false))
    assert(createMockTask(taskXML7).fold(de => de == XMLError("Attribute time is empty/undefined in Task"), _ => false))
    assert(createMockTask(taskXML8).fold(de => de == XMLError("Attribute type is empty/undefined in PhysicalResource"), _ => false))
    assert(createMockTask(taskXML9).fold(de => de == TaskUsesNonExistentPRT("PRST 2"), _ => false))
    assert(createMockTask(taskXML10).fold(de => de == TaskUsesNonExistentPRT("PRST 2"), _ => false))

  }


object PhysicalResourcesMock:
  private def createMockPhysicalResource(id: String, phyType: String): Result[PhysicalResource] =
    for {
      prId <- PhysicalResourceId.from(id)
      prType <- PhysicalResourceType.from(phyType)
    } yield PhysicalResource(prId, prType)

  def createMockPhysicalResources: Result[List[PhysicalResource]] =
    for {
      p1 <- createMockPhysicalResource("PRS_1", "PRST 1")
      p2 <- createMockPhysicalResource("PRS_4", "PRST 4")
    } yield List(p1, p2)

  def createMockPhysicalResources1: Result[List[PhysicalResource]] =
    for {
      p1 <- createMockPhysicalResource("PRS_1", "PRST 1")
      p2 <- createMockPhysicalResource("PRS_2", "PRST 2")
      p3 <- createMockPhysicalResource("PRS_3", "PRST 3")
      p4 <- createMockPhysicalResource("PRS_4", "PRST 4")
      p5 <- createMockPhysicalResource("PRS_5", "PRST 5")
    } yield List(p1, p2, p3, p4, p5)

object HumanResourcesMock:
  private def createMockHumanResource(id: String, name: String, handles: List[PhysicalResourceType]): Result[HumanResource] =
    for {
      hrId <- HumanResourceId.from(id)
      hrName <- NonEmptyString.from(name)
    } yield HumanResource(hrId, hrName, handles)

  def createMockHumanResources: Result[List[HumanResource]] =
    for {
      pr1type <- PhysicalResourceType.from("PRST 1")
      p1 <- createMockHumanResource("HRS_1", "Antonio", List(pr1type))
      pr2type <- PhysicalResourceType.from("PRST 4")
      p2 <- createMockHumanResource("HRS_4", "Maria", List(pr2type))
    } yield List(p1, p2)

  def createMockHumanResources1: Result[List[HumanResource]] =
    val x: String = "PRST 5"
    for {
      pr1type <- PhysicalResourceType.from("PRST 1")
      p1 <- createMockHumanResource("HRS_1", "PRST 1", List(pr1type))
      pr2type <- PhysicalResourceType.from("PRST 2")
      p2 <- createMockHumanResource("HRS_2", "PRST 2", List(pr2type))
      pr3type <- PhysicalResourceType.from("PRST 3")
      p3 <- createMockHumanResource("HRS_3", "PRST 3", List(pr3type))
      pr4type <- PhysicalResourceType.from("PRST 4")
      p4 <- createMockHumanResource("HRS_4", "PRST 4", List(pr4type))
      pr5type <- PhysicalResourceType.from("PRST 5")
      p5 <- createMockHumanResource("HRS_5", "PRST 5", List(pr5type))
    } yield List(p1, p2, p3, p4, p5)

object TaskMock:

  def createMockTask(xml: Node): Result[Task] =
    for {
      prList <- PhysicalResourcesMock.createMockPhysicalResources
      hrList <- HumanResourcesMock.createMockHumanResources
      result <- Task.from(prList, hrList)(xml)
    } yield result

  def createMockTask1(xml: Node): Result[Task] =
    for {
      prList <- PhysicalResourcesMock.createMockPhysicalResources1
      hrList <- HumanResourcesMock.createMockHumanResources
      result <- Task.from(prList, hrList)(xml)
    } yield result