package domain.domain

import org.scalatest.funsuite.AnyFunSuite

import scala.language.adhocExtensions
import scala.xml.Elem
import domain.domain.*
import domain.Result
import domain.domain.SimpleTypes.*

class TaskScheduleTest extends AnyFunSuite :

  test("Should convert a valid domain entity Physical Resource to the respective XML element") {
    
    val task1= for{
      id <-  TaskId.from("TSK_1")
      duration <- TaskDuration.from("30")
      prt <- PhysicalResourceType.from("PRST_1")
    } yield Task(id, duration, List(prt))
    
    val product= for {
      id <- ProductId.from("PRD_1")
      fullName <- NonEmptyString.from("Product 1")
      tsk <- task1
    } yield Product(id,fullName,List(tsk))
    
    val order = for{
     id <- OrderId.from("ORD_1")
     p <- product
     quantity <- Quantity.from("1") 
    } yield Order(id,p,quantity)

    val physicalResource1 = for {
      prId <- PhysicalResourceId.from("PRS_1")
      prType <- PhysicalResourceType.from("PRST_1")
    } yield PhysicalResource(prId, prType)

    val physicalResource2 = for {
      prId <- PhysicalResourceId.from("PRS_2")
      prType <- PhysicalResourceType.from("PRST_2")
    } yield PhysicalResource(prId, prType)

    val pr1 = PhysicalResourceType.from("PRST_10")
    val pr4 = PhysicalResourceType.from("PRST_4")

    val listPR =
      for {
        i1 <- pr1
        i2 <- pr4
      } yield List(i1, i2)


    val humanResource1 =
      for {
        hrId <- HumanResourceId.from("HRS_1")
        hrName <- NonEmptyString.from("Antonio")
        handles <- listPR
      } yield HumanResource(hrId, hrName, handles)

    val humanResource2 =
      for {
        hrId <- HumanResourceId.from("HRS_1")
        hrName <- NonEmptyString.from("Maria")
        handles <- listPR
      } yield HumanResource(hrId, hrName, handles)

    val result: Result[TaskSchedule] =
      for {
        ord <- order
        task<- task1
        start <- TaskInstant.from(0)
        end <- TaskInstant.from(100)
        pr1 <- physicalResource1
        pr2 <- physicalResource2
        hr1 <- humanResource1
        hr2 <- humanResource2
      } yield TaskSchedule(ord, 1, task, start, end, List(pr1, pr2), List(hr1, hr2))

    val expected: Elem = <TaskSchedule order="ORD_1" productNumber="1" 
                                       task="TSK_1" start="0" end="100">
      <PhysicalResources>
        <Physical id="PRS_1"/><Physical id="PRS_2"/>
      </PhysicalResources>
      <HumanResources>
        <Human name="Antonio"/><Human name="Maria"/>
      </HumanResources>
    </TaskSchedule>

    assert(result.fold(_ => false, ts => TaskSchedule.toXML(ts) == expected))
  }
  

