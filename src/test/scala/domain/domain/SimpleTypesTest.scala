package domain.domain

import domain.domain.SimpleTypes
import domain.DomainError
import domain.Result
import domain.domain.SimpleTypes.*
import org.scalatest.funsuite.AnyFunSuite

import scala.language.adhocExtensions

class SimpleTypesTest extends AnyFunSuite :

  test("Should accept valid Physical Resource IDs") {
    val ls = List("PRS_1", "PRS_2", "PRS_3", "PRS_6136126", "PRS_XXXX", "PRS_PP", "PRS_Teste")
    ls.foreach(s =>
      assert(PhysicalResourceId.from(s).fold(de => false, result => result.to == s))
    )
  }

  test("Should not accept invalid Physical Resource IDs") {
    val ls = List("PR_1", "PRS", "PRS3", "_2", "PRS-", "PRS-10", "PR-11")
    ls.foreach(s =>
      assert(PhysicalResourceId.from(s).fold(de => de == DomainError.InvalidPhysicalId(s), _ => false))
    )
  }

  test("Should accept valid Task IDs") {
    val ls = List("TSK_1", "TSK_2", "TSK_3", "TSK_4", "TSK_5")
    ls.foreach(s =>
      assert(TaskId.from(s).fold(de => false, result => result.to == s))
    )
  }

  test("Should not accept invalid Task IDs") {
    val ls = List("TSK", "TK_2", "TS_3", "SK_4", "TSK5")
    ls.foreach(s =>
      assert(TaskId.from(s).fold(de => de == DomainError.InvalidTaskId(s), _ => false))
    )
  }

  test("Should accept valid Human IDs") {
    val ls = List("HRS_1", "HRS_2", "HRS_3", "HRS_4", "HRS_5")
    ls.foreach(s =>
      assert(HumanResourceId.from(s).fold(de => false, result => result.to == s))
    )
  }

  test("Should not accept invalid Human IDs") {
    val ls = List("HRS", "HS_2", "HR_3", "RS_4", "HRS5")
    ls.foreach(s =>
      assert(HumanResourceId.from(s).fold(de => de == DomainError.InvalidHumanId(s), _ => false))
    )
  }

  test("Should accept valid Product Ids") {
    val lp = List("PRD_1", "PRD_2", "PRD_3", "PRD_6136126", "PRD_XXXX", "PRD_PP", "PRD_Teste")
    lp.foreach(s =>
      assert(ProductId.from(s).fold(de => false, result => result.to == s))
    )
  }

  test("Should not accept invalid Product Ids") {
    val ls = List("PRd_1", "PRD", "PRD3", "1_PRD", "PROD", "")
    ls.foreach(s =>
      assert(ProductId.from(s).fold(de => de == DomainError.InvalidProductId(s), _ => false))
    )
  }

  test("Should accept valid Order Ids") {

    val lo = List("ORD_1", "ORD_2", "ORD_*", "ORD_6136126", "ORD_", "ORD_PP", "ORD_Teste")
    lo.foreach(s =>
      assert(OrderId.from(s).fold(de => false, result => result.to == s))
    )

  }

  test("Should not accept invalid OrderId") {

    val lo = List("OR_1", "ORD", "ORD3", "2ORD_", "ORD-10", "ORD-11")
    lo.foreach(s =>
      assert(OrderId.from(s).fold(de => de == DomainError.InvalidOrderId(s), _ => false))
    )

  }


  test("Shoul accept NonEmptyString") {

    val ls = List("Product", "2", "_", "a", "AaaSddSggD", "Product x", "*-")
    ls.foreach(s =>
      assert(NonEmptyString.from(s).fold(de => false, result => result.to == s))
    )
  }

  test("Should not accept EmptyString") {
    assert(NonEmptyString.from("").fold(de => de == DomainError.EmptyString, _ => false))
  }

  test("Should accept valid quantities") {
    val lp = List("1", "2", "3", "4000")
    lp.foreach(s =>
      assert(Quantity.from(s).fold(de => false, result => result.to == s.toInt))
    )
  }

  test("Should not accept invalid quantities") {
    val lq = List("0", "-1", "-2", "o", "")
    lq.foreach(s =>
      assert(Quantity.from(s).fold(de => de == DomainError.InvalidQuantity(s), _ => false))
    )
  }

  test("Test Instant plus Duration ") {
    
    val instant10= 10
    val duration20="20"
    val expected= 30
    val result = 
      for{
        i <- TaskInstant.from(instant10)
        d <- TaskDuration.from(duration20)
      } yield (i + d)
    assert( result.fold(de => false, i => i.to == expected) )
  }

  test("Test Instant minus Duration ") {

    val instant30= 30
    val duration20="20"
    val expected= 10
    val result =
      for{
        i <- TaskInstant.from(instant30)
        d <- TaskDuration.from(duration20)
        r <- i - d
      } yield r
    assert( result.fold(de => false, i => i.to == expected) )
  }