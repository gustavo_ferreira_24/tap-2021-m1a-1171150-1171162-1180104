package domain.domain

import scala.language.adhocExtensions
import org.scalatest.funsuite.AnyFunSuite

import scala.xml.Node
import xml.XML.*
import domain.domain.TaskMock.*
import domain.domain.SimpleTypes.*
import domain.DomainError.*
import domain.Result

import scala.collection.View.Empty

class ProductTest extends AnyFunSuite :

  import domain.domain.ProductMock

  test("Valid Product node should result in a valid Product instance") {

    val tasksNode1: Node = <Tasks>
      <Task id="TSK_1" time="100">
        <PhysicalResource type="PRST 1"/>
        <PhysicalResource type="PRST 4"/>
      </Task>
      <Task id="TSK_2" time="80">
        <PhysicalResource type="PRST 2"/>
        <PhysicalResource type="PRST 5"/>
      </Task>
    </Tasks>

    val tasksNode2: Node = <Tasks>
      <Task id="TSK_2" time="80">
        <PhysicalResource type="PRST 2"/>
        <PhysicalResource type="PRST 5"/>
      </Task>
    </Tasks>

    val productXML1: Node =
      <Product id="PRD_1" name="Product 1">
        <Process tskref="TSK_1"/>
        <Process tskref="TSK_2"/>
      </Product>

    val productXML2: Node = <Product id="PRD_2" name="Product 2">
      <Process tskref="TSK_2"/>
    </Product>


    val expected1: Result[Product] = for {
      id <- ProductId.from("PRD_1")
      name <- NonEmptyString.from("Product 1")
      tasks <- traverse((tasksNode1 \\ "Task"), createMockTask1)
    } yield Product(id, name, tasks)

    val expected2: Result[Product] = for {
      id <- ProductId.from("PRD_2")
      name <- NonEmptyString.from("Product 2")
      tasks <- traverse((tasksNode2 \\ "Task"), createMockTask1)
    } yield Product(id, name, tasks)

    assert(ProductMock.createMockProduct(productXML1) == expected1)
    assert(ProductMock.createMockProduct(productXML2) == expected2)

  }

  test("Product node with invalid task reference should result in a Domain Error") {

    val productXML3: Node = <Product id="PRD_1" name="Product 1">
      <Process tskref="TSK_1"/>
      <Process tskref="TSK_99"/>
    </Product>

    val productXML4: Node = <Product id="PRD_2" name="Product 2">
      <Process tskref="TSK_3"/>
    </Product>

    assert(ProductMock.createMockProduct(productXML3).fold(de => de == TaskDoesNotExist("TSK_99"), _ => false))
    assert(ProductMock.createMockProduct(productXML4).fold(de => de == TaskDoesNotExist("TSK_3"), _ => false))


  }

  test("Product node with invalid attributes should result in a XMLError") {

    val productXML5: Node = <Product id="PRD_1" name="Product 1">
      <Process tskref="TSK_1"/>
      <Process tsk="TSK_99"/>
    </Product>
    val productXML6: Node = <Product id="PRD_1" name="">
      <Process tskref="TSK_1"/>
      <Process tskref="TSK_99"/>
    </Product>
    val productXML7: Node = <Product ide="PRD_2" name="Product 2">
      <Process tskref="TSK_3"/>
    </Product>
    val productXML8: Node = <Product id="PROD_1" name="Product 1">
      <Process tskref="TSK_1"/>
      <Process tskref="TSK_2"/>
    </Product>
    

    assert(ProductMock.createMockProduct(productXML5).
      fold(de => de == XMLError("Attribute tskref is empty/undefined in Process"), _ => false))
    assert(ProductMock.createMockProduct(productXML6).
      fold(de => de == XMLError("Attribute name is empty/undefined in Product"), _ => false))
    assert(ProductMock.createMockProduct(productXML7).
      fold(de => de == XMLError("Attribute id is empty/undefined in Product"), _ => false))
    assert(ProductMock.createMockProduct(productXML8).fold(de => de == InvalidProductId("PROD_1"), _ => false))

  }

object ProductMock:
  val tasksNode1: Node = <Tasks>
    <Task id="TSK_1" time="100">
      <PhysicalResource type="PRST 1"/>
      <PhysicalResource type="PRST 4"/>
    </Task>
    <Task id="TSK_2" time="80">
      <PhysicalResource type="PRST 2"/>
      <PhysicalResource type="PRST 5"/>
    </Task>
  </Tasks>

  def createMockProduct(xml: Node): Result[Product] =
    for {
      tskList <- traverse((tasksNode1 \\ "Task"), createMockTask1)
      result <- Product.from(tskList)(xml)
    } yield result
