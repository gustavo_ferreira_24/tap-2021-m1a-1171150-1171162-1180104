package domain.domain

import domain.domain.SimpleTypes.*
import domain.DomainError
import domain.Result
import org.scalatest.funsuite.AnyFunSuite

import scala.language.adhocExtensions
import scala.xml.Node

class PhysicalResourceTest extends AnyFunSuite :

  test("Should convert a valid PhysicalResource in XML format to the internal domain object") {
    val physicalResourceXML1 = <Physical id="PRS_1" type="PRST 1"/>
    val physicalResourceXML2 = <Physical id="PRS_2" type="PRST 3"/>
    val physicalResourceXML3 = <Physical id="PRS_45" type="Type X"/>

    assert(PhysicalResource.from(physicalResourceXML1).fold(de => false, result => result.id.to == "PRS_1" && result.phyResourceType.to == "PRST 1"))
    assert(PhysicalResource.from(physicalResourceXML2).fold(de => false, result => result.id.to == "PRS_2" && result.phyResourceType.to == "PRST 3"))
    assert(PhysicalResource.from(physicalResourceXML3).fold(de => false, result => result.id.to == "PRS_45" && result.phyResourceType.to == "Type X"))
  }

  test("Should not convert an invalid PhysicalResource in XML format to the respective Domain Error") {
    val physicalResourceXML1 = <Physical id="PR_1" type="PRST 1"/>
    val physicalResourceXML2 = <Physical id="PRS-2" type="PRST 3"/>
    val physicalResourceXML3 = <Physical id="PRS3" type="Type X"/>
    val physicalResourceXML4 = <Physical idd="PRS3" type="Type Y"/>
    val physicalResourceXML5 = <Physical id="PRS_3" typ1e="Type Z"/>

    assert(PhysicalResource.from(physicalResourceXML1).fold(de => de == DomainError.InvalidPhysicalId("PR_1"), _ => false))
    assert(PhysicalResource.from(physicalResourceXML2).fold(de => de == DomainError.InvalidPhysicalId("PRS-2"), _ => false))
    assert(PhysicalResource.from(physicalResourceXML3).fold(de => de == DomainError.InvalidPhysicalId("PRS3"), _ => false))
    assert(PhysicalResource.from(physicalResourceXML4).fold(de => de == DomainError.XMLError("Attribute id is empty/undefined in Physical"), _ => false))
    assert(PhysicalResource.from(physicalResourceXML5).fold(de => de == DomainError.XMLError("Attribute type is empty/undefined in Physical"), _ => false))
  }

  test("Should convert a valid domain entity Physical Resource to the respective XML element") {

    val physicalResource1 = for {
      prId <- PhysicalResourceId.from("PRS_1")
      prType <- PhysicalResourceType.from("PRST_1")
    } yield PhysicalResource(prId, prType)

    val physicalResource2 = for {
      prId <- PhysicalResourceId.from("PRS_2")
      prType <- PhysicalResourceType.from("PRST_2")
    } yield PhysicalResource(prId, prType)

    assert(physicalResource1.fold(_ => false, pr => PhysicalResource.toXML(pr) == <Physical id="PRS_1"/>))
    assert(physicalResource2.fold(_ => false, pr => PhysicalResource.toXML(pr) == <Physical id="PRS_2"/>))
  }