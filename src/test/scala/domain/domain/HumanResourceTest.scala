package domain.domain

import domain.domain.SimpleTypes.*
import domain.DomainError
import domain.Result
import domain.domain.PhysicalResourcesMock.*

import scala.language.adhocExtensions
import org.scalatest.funsuite.AnyFunSuite
import scala.xml.Node

class HumanResourceTest extends AnyFunSuite :

  test("Should convert a valid Human Resource in XML format to the internal domain object") {
    val humanResourceXML1 = <Human id="HRS_1" name="Antonio">
      <Handles type="PRST 1"/>
      <Handles type="PRST 4"/>
    </Human>
    val humanResourceXML2 = <Human id="HRS_2" name="Maria">
      <Handles type="PRST 4"/>
    </Human>

    assert(createMockHumanResource(humanResourceXML1).fold(de => false, result =>
      result.id.to == "HRS_1" && result.name.to == "Antonio" && result.handles.equals(List(("PRST 1"), ("PRST 4")))))

    assert(createMockHumanResource(humanResourceXML2).fold(de => false, result =>
      result.id.to == "HRS_2" && result.name.to == "Maria" && result.handles.equals(List(("PRST 4")))))
  }

  test("Should not convert an invalid Human Resource in XML format to the respective Domain Error") {
    val humanResourceXML1 = <Human id="HR_1" name="Antonio">
      <Handles type="PRST 1"/>
      <Handles type="PRST 4"/>
    </Human>
    val humanResourceXML2 = <Human id="HRS-2" name="Maria">
      <Handles type="PRST 4"/>
    </Human>
    val humanResourceXML3 = <Human id="HRS3" name="Pedro">
      <Handles type="PRST 4"/>
    </Human>
    val humanResourceXML4 = <Human idd="HRS_1" name="Carlos">
      <Handles type="PRST 4"/>
    </Human>
    val humanResourceXML5 = <Human id="HRS_3" namse="Roberto">
      <Handles type="PRST 4"/>
    </Human>
    val humanResourceXML6 = <Human id="HRS_5" name="Nuno">
      <Handles type1="PRST 4"/>
    </Human>
    val humanResourceXML7 = <Human id="HRS_5" name="Carolina">
      <Handles type="PRST 2"/>
    </Human>
    val humanResourceXML8 = <Human id="HRS_1" name="Beatriz">
      <Handles type="PRST 1"/>
      <Handles type="PRST 2"/>
    </Human>


    assert(createMockHumanResource(humanResourceXML1).fold(de => de == DomainError.InvalidHumanId("HR_1"), _ => false))
    assert(createMockHumanResource(humanResourceXML2).fold(de => de == DomainError.InvalidHumanId("HRS-2"), _ => false))
    assert(createMockHumanResource(humanResourceXML3).fold(de => de == DomainError.InvalidHumanId("HRS3"), _ => false))
    assert(createMockHumanResource(humanResourceXML4).fold(de => de == DomainError.XMLError("Attribute id is empty/undefined in Human"), _ => false))
    assert(createMockHumanResource(humanResourceXML5).fold(de => de == DomainError.XMLError("Attribute name is empty/undefined in Human"), _ => false))
    assert(createMockHumanResource(humanResourceXML6).fold(de => de == DomainError.XMLError("Attribute type is empty/undefined in Handles"), _ => false))
    assert(createMockHumanResource(humanResourceXML7).fold(de => de == DomainError.HumanUsesNonExistentPRT("PRST 2"), _ => false))
    assert(createMockHumanResource(humanResourceXML8).fold(de => de == DomainError.HumanUsesNonExistentPRT("PRST 2"), _ => false))
  }

  test("Should convert a valid domain entity Human Resource to the respective XML element") {
    val pr1 = PhysicalResourceType.from("PRST_10")
    val pr4 = PhysicalResourceType.from("PRST_4")

    val listPR =
      for {
        i1 <- pr1
        i2 <- pr4
      } yield List(i1, i2)


    val humanResource1 =
      for {
        hrId <- HumanResourceId.from("HRS_1")
        hrName <- NonEmptyString.from("Antonio")
        handles <- listPR
      } yield HumanResource(hrId, hrName, handles)

    val humanResource2 =
      for {
        hrId <- HumanResourceId.from("HRS_1")
        hrName <- NonEmptyString.from("Maria")
        handles <- listPR
      } yield HumanResource(hrId, hrName, handles)

    assert(humanResource1.fold(_ => false, hr => HumanResource.toXML(hr) == <Human name="Antonio"/>))
    assert(humanResource2.fold(_ => false, hr => HumanResource.toXML(hr) == <Human name="Maria"/>))
  }


  def createMockHumanResource(humanResourceXML: Node) =
    for
      pr <- createMockPhysicalResources
      hr <- HumanResource.from(pr)(humanResourceXML)
    yield hr
