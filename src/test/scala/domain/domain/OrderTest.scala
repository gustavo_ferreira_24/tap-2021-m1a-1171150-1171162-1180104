package domain.domain

import scala.language.adhocExtensions
import org.scalatest.funsuite.AnyFunSuite

import scala.xml.Node
import xml.XML.*
import domain.domain.SimpleTypes.*
import domain.DomainError.*
import domain.Result
import domain.domain.OrderMock.*
import domain.domain.ProductMock.*


class OrderTest extends AnyFunSuite :

  test("Valid Order") {

    val xml= <Production>
      <Products><Product id="PRD_1" name="Product 1"><Process tskref="TSK_1"/><Process tskref="TSK_2"/>
    </Product><Product id="PRD_2" name="Product 2"><Process tskref="TSK_2"/></Product></Products>
    <Orders><Order id="ORD_1" prdref="PRD_1" quantity="1"/>
      <Order id="ORD_2" prdref="PRD_2" quantity="2"/></Orders>
    </Production>
    
    val prodXML1= <Product id="PRD_1" name="Product 1"><Process tskref="TSK_1"/><Process tskref="TSK_2"/></Product>
    val prodXML2= <Product id="PRD_2" name="Product 2"><Process tskref="TSK_2"/></Product>
    
    val orderXML1= <Order id="ORD_1" prdref="PRD_1" quantity="1"/>
    val orderXML2= <Order id="ORD_2" prdref="PRD_2" quantity="2"/>
    
    val order1=createMockOrder(orderXML1)
    val order2=createMockOrder(orderXML1)
    
    val result1: Result[Order]=for{
      id <- OrderId.from("ORD_1")
      prod <- createMockProduct(prodXML1)
      q <- Quantity.from("1")
    } yield Order(id, prod, q)

    val result2: Result[Order] = for {
      id <- OrderId.from("ORD_2")
      prod <- createMockProduct(prodXML2)
      q <- Quantity.from("2")
    } yield Order(id, prod, q)

    assert(createMockOrder(orderXML1) == result1)
    assert(createMockOrder(orderXML2) == result2)

  }

  test("Order with an invalid product reference"){
    
    val order= <Orders><Order id="ORD_1" prdref="PRD_1" quantity="1"/>
      <Order id="ORD_2" prdref="PRD_99" quantity="2"/></Orders>
    
    val product= <Products>
    <Product id="PRD_1" name="Product 1"><Process tskref="TSK_1"/><Process tskref="TSK_2"/></Product>
      <Product id="PRD_2" name="Product 2"><Process tskref="TSK_2"/></Product></Products>
    
    val orderXML3= <Order id="ORD_1" prdref="PRD_10" quantity="1"/>
    val orderXML4= <Order id="ORD_2" prdref="PRD_99" quantity="2"/>

    assert(createMockOrder(orderXML3).fold(de => de == ProductDoesNotExist("PRD_10"), _ => false))
    assert(createMockOrder(orderXML4).fold(de => de == ProductDoesNotExist("PRD_99"), _ => false))

  }


  test("Order with invalid attributes"){

    val orderXML5= <Order id="ORD_1" prdref="PRD_1" />
    val orderXML6= <Order id="ORD_1" ref="PRD_1" quantity="1"/>
    val orderXML7= <Order prdref="PRD_2" quantity="2"/>
    val orderXML8= <Order id="ORDER_1" prdref="PRD_1" quantity="1"/>
    val orderXML9= <Order id="ORD_2" prdref="PRD_1" quantity="0"/>

    assert(createMockOrder(orderXML5).fold(de => de == XMLError("Attribute quantity is empty/undefined in Order"), _ => false))
    assert(createMockOrder(orderXML6).fold(de => de == XMLError("Attribute prdref is empty/undefined in Order"), _ => false))
    assert(createMockOrder(orderXML7).fold(de => de == XMLError("Attribute id is empty/undefined in Order"), _ => false))
    assert(createMockOrder(orderXML8).fold(de => de == InvalidOrderId("ORDER_1"), _ => false))
    assert(createMockOrder(orderXML9).fold(de => de == InvalidQuantity("0"), _ => false))


  }

object OrderMock:

  val productsXML = <Products>
    <Product id="PRD_1" name="Product 1">
      <Process tskref="TSK_1"/>
      <Process tskref="TSK_2"/>
    </Product>
    <Product id="PRD_2" name="Product 2">
      <Process tskref="TSK_2"/>
    </Product>
  </Products>

  def createMockOrder(xml: Node): Result[Order] =
    for
      prodList <- traverse((productsXML \\ "Product"), createMockProduct)
      order <- Order.from(prodList)(xml)
    yield order
