package domain.domain

import domain.Result
import domain.DomainError.*
import domain.domain.SimpleTypes.*
import xml.XML.*
import scala.collection.immutable.*
import scala.xml.Node
import scala.xml.NodeSeq

final case class Task(id: TaskId, duration: TaskDuration, physicalResourceTypes: List[PhysicalResourceType])

object Task:
  def from(lr: List[PhysicalResource], hr: List[HumanResource])(xml: Node): Result[Task] =
    for
      tid <- fromAttribute(xml, "id")
      id <- TaskId.from(tid)
      tduration <- fromAttribute(xml, "time")
      duration <- TaskDuration.from(tduration)
      resources <- traverse((xml \\ "PhysicalResource"), TaskPhysicalResourceType.from(lr)(id))
    yield Task(id, duration, resources)

final case class TaskPhysicalResourceType(resourceType: PhysicalResourceType)

object TaskPhysicalResourceType:
  def from(lr: List[PhysicalResource])(id: TaskId)(xml: Node): Result[PhysicalResourceType] =
    for
      resourcetype <- fromAttribute(xml, "type")
      phyresourcetype <- PhysicalResourceType.from(resourcetype)
      foundResource <- lr.find(r => r.phyResourceType == phyresourcetype).fold(Left(TaskUsesNonExistentPRT(phyresourcetype.to)))(r => Right(r.phyResourceType))
    yield foundResource
