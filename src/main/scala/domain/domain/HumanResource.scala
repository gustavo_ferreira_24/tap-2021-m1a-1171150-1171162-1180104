package domain.domain

import domain.DomainError.*
import domain.Result
import domain.domain.SimpleTypes.*
import domain.domain.Resource
import xml.XML.*
import scala.collection.immutable.*
import scala.xml.{Elem, Node}
import scala.xml.NodeSeq

final case class HumanResource(id: HumanResourceId, name: NonEmptyString, handles: List[PhysicalResourceType]) extends Resource:

  def hasResourceType(resourceType: PhysicalResourceType)=
    this.handles.find(handle => handle == resourceType).isDefined

object HumanResource:
  def from(lr: List[PhysicalResource])(xml: Node): Result[HumanResource] =
    for
      hid <- fromAttribute(xml, "id")
      id <- HumanResourceId.from(hid)
      hName <- fromAttribute(xml, "name")
      name <-  NonEmptyString.from(hName)
      handles <- traverse((xml \\ "Handles"), HumanResourcePhyType.from(lr))
    yield HumanResource(id, name, handles)

  def toXML(humanResource: HumanResource): Elem = <Human name={humanResource.name.to}/>

final case class HumanResourcePhyType(phyType: PhysicalResourceType)

object HumanResourcePhyType:
  def from(lr: List[PhysicalResource])(xml: Node): Result[PhysicalResourceType] =
    for
      resourcetype <- fromAttribute(xml, "type")
      phyresourcetype <- PhysicalResourceType.from(resourcetype)
      foundResources <- lr.find(r => r.phyResourceType == phyresourcetype).fold(Left(HumanUsesNonExistentPRT(phyresourcetype.to)))(r => Right(r.phyResourceType))
    yield foundResources
