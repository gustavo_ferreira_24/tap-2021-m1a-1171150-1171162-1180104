package domain.domain

import domain.Result
import domain.domain.SimpleTypes.*
import domain.domain.*
import scala.xml.Elem

final case class TaskSchedule(order: Order, productNumber: Int, task: Task,
                              startingTime: TaskInstant, finalTime: TaskInstant,
                              physicalResources: List[PhysicalResource],
                              humanResources: List[HumanResource])

object TaskSchedule:

  def toXML(taskSchedule: TaskSchedule): Elem =
    <TaskSchedule order={taskSchedule.order.orderId.to} productNumber={taskSchedule.productNumber.toString} 
                  task={taskSchedule.task.id.to} start={taskSchedule.startingTime.to.toString} end={taskSchedule.finalTime.to.toString}>
      <PhysicalResources>
        {taskSchedule.physicalResources.map(PhysicalResource.toXML(_))}
      </PhysicalResources>
      <HumanResources>
        {taskSchedule.humanResources.map(HumanResource.toXML(_))}
      </HumanResources>
    </TaskSchedule>
