package domain.domain

import domain.domain.*
import domain.Result
import scala.xml.*

case class ScheduleDomain(taskScheduleList: List[TaskSchedule])

object ScheduleDomain:
  def toXML(scheduleDomain: ScheduleDomain): Result[Elem] =
    Right(<Schedule xmlns="http://www.dei.isep.ipp.pt/tap-2021" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2021 ../../schedule.xsd ">
      {scheduleDomain.taskScheduleList.map(ts => TaskSchedule.toXML(ts))}
    </Schedule>)