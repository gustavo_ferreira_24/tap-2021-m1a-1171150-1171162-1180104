package domain.domain

import domain.Result
import domain.domain.SimpleTypes.*
import scala.xml.{Elem, Node}
import domain.domain.Resource
import xml.XML.*

final case class PhysicalResource(id: PhysicalResourceId, phyResourceType: PhysicalResourceType) extends Resource:
  
  def hasResourceType(resourceType: PhysicalResourceType)=
    this.phyResourceType==resourceType

object PhysicalResource:
  def from(xml: Node): Result[PhysicalResource] =
    for
      sid <- fromAttribute(xml, "id")
      id <- PhysicalResourceId.from(sid)
      sPhyResourceType <- fromAttribute(xml, "type")
      phyResourceType <- PhysicalResourceType.from(sPhyResourceType)
    yield (PhysicalResource(id, phyResourceType))

  def toXML(physicalResource: PhysicalResource): Elem = <Physical id={physicalResource.id.to}/>
