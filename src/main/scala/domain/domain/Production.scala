package domain.domain

import domain.Result
import domain.domain.SimpleTypes.*

import scala.xml.Node
import xml.XML.*

final case class Production(physicalResources: Seq[PhysicalResource], tasks: Seq[Task],
                            humanResources: Seq[HumanResource], products: Seq[Product], orders: Seq[Order])

object Production:
  def from(xml: Node): Result[Production] =
    for
      physicalResources <- fromNode(xml, "PhysicalResources")
      phyResources <- traverse((physicalResources \\ "Physical"), PhysicalResource.from)
      humanResources <- fromNode(xml, "HumanResources")
      hmnResources <- traverse((humanResources \\ "Human"), HumanResource.from(phyResources))
      tasks <- fromNode(xml,"Tasks")
      tsks <- traverse((tasks \\ "Task"),Task.from(phyResources,hmnResources))
      prod<- fromNode(xml,"Products")
      products<- traverse((prod \\ "Product"), Product.from(tsks))
      ord<- fromNode(xml,"Orders")
      orders<- traverse((ord \\ "Order"),Order.from(products))
    yield (Production(phyResources, tsks, hmnResources, products, orders))
