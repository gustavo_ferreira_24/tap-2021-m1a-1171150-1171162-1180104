package domain.domain

import domain.Result
import domain.domain.SimpleTypes.*
import domain.DomainError.*

import scala.xml.Node
import xml.XML.*

final case class Product(productId: ProductId, fullName: NonEmptyString, tasksList: List[Task])

object Product:

  private def fromTask(lt: List[Task])(xml: Node): Result[Task] =
    for {
      tskref <- fromAttribute(xml, "tskref")
      taskId <- TaskId.from(tskref)
      task <- lt.find(t => t.id == taskId).fold(Left(TaskDoesNotExist(tskref)))(t => Right(t))
    } yield task

  def from(lt: List[Task])(xml: Node): Result[Product] =
    for {
      id <- fromAttribute(xml, "id")
      productId <- ProductId.from(id)
      name <- fromAttribute(xml, "name")
      fullName <- NonEmptyString.from(name)
      tasks <- traverse((xml \\ "Process"), fromTask(lt))
    } yield Product(productId, fullName, tasks)
