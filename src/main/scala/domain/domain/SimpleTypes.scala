package domain.domain

import domain.Result
import domain.DomainError.*
import scala.annotation.targetName
import scala.util.matching.Regex

import scala.util.Try

object SimpleTypes:

  opaque type ProductId = String

  object ProductId:
    def from(s: String): Result[ProductId] =
      val rx: Regex = "PRD_.*".r()
      if (rx.matches(s)) Right(s) else Left(InvalidProductId(s))

  extension (p: ProductId)
    @targetName("ProductTo")
    def to: String = p

  opaque type OrderId = String

  object OrderId:
    def from(s: String): Result[OrderId] =
      val rx: Regex = "ORD_.*".r()
      if (rx.matches(s)) Right(s) else Left(InvalidOrderId(s))

  extension (o: OrderId)
    @targetName("OrderTo")
    def to: String = o
    @targetName("OrderIdGetOrderIdWithoutQuantity")
    def getOrderIdWithoutQuantity: String = o.split("_")(0) + "_" + o.split("_")(1)

  opaque type NonEmptyString = String

  object NonEmptyString:
    def from(s: String): Result[NonEmptyString] =
      if (s.isEmpty) Left(EmptyString) else Right(s)

  extension (n: NonEmptyString)
    @targetName("NonEmptyStringTo")
    def to: String = n

  opaque type Quantity = Int

  object Quantity:
    def from(s: String): Result[Quantity] = 
      val i: Int = Try(s.toInt).fold[Int](_ => 0, i => i)
      if (i > 0) Right(i) else Left(InvalidQuantity(s))
    def unit: Quantity = 1
  
  extension (q: Quantity)
    @targetName("QuantityTo")
    def to: Int = q
    @targetName("QuantityInc")
    def inc: Quantity= q + 1
    @targetName("SumQuantity")
    infix def +(q2:Quantity):Quantity= q + q2
    @targetName("QuantityEquals")
    def equals(q2: Quantity): Boolean= q==q2 
  
  
  opaque type TaskId = String

  object TaskId:
    def from(id: String): Result[TaskId] =
      val pattern = "TSK_.*".r
      if (pattern.matches(id)) Right(id) else Left(InvalidTaskId(id))

  extension (taskId: TaskId)
    @targetName("taskIdTo")
    def to: String = taskId

  opaque type TaskDuration = Int

  object TaskDuration:
    def from(duration: String): Result[TaskDuration] =
      val intDuration = Try(duration.toInt).fold[Int](_ => 0, i => i)
      if (intDuration > 0) Right(intDuration) else Left(InvalidTaskTime(duration))
    def unit: TaskDuration= 1
  
  extension (taskDuration: TaskDuration)
    @targetName("taskDurationTo")
    def to: Int = taskDuration


  opaque type TaskInstant = Int

  object TaskInstant:
    def from(instant: Int): Result[TaskInstant] =
      if (instant >= 0) Right(instant) else Left(InvalidTaskTime(instant.toString))
    def unit: TaskInstant= 1
    def zero: TaskInstant= 0

  extension (taskInstant: TaskInstant)
    @targetName("taskInstantTo")
    def to: Int = taskInstant
    @targetName("taskInstantPlusDuration")
    infix def +(duration: TaskDuration): TaskInstant= taskInstant + duration
    @targetName("taskInstantMinusDuration")
    infix def -(duration: TaskDuration): Result[TaskInstant] =
      val i = taskInstant - duration
      if (i<0) Left(InvalidTaskTime(i.toString)) else Right(i)
    @targetName("isBefore")
    infix def < (taskInstant2: TaskInstant): Boolean = (taskInstant < taskInstant2)
    @targetName("isAfter")
    infix def > (taskInstant2: TaskInstant): Boolean = (taskInstant > taskInstant2)

  opaque type PhysicalResourceId = String

  object PhysicalResourceId:
    def from(s: String): Result[PhysicalResourceId] =
      val regex = "PRS_.*".r
      if (regex.matches(s)) Right(s) else Left(InvalidPhysicalId(s))

  extension (physicalResourceId: PhysicalResourceId)
    @targetName("phyResourceIdTo")
    def to: String = physicalResourceId

  opaque type PhysicalResourceType = String

  object PhysicalResourceType:
    def from(s: String): Result[PhysicalResourceType] = 
      if (s.isEmpty) Left(EmptyString) else Right(s)

  extension (physicalResourceType: PhysicalResourceType)
    @targetName("phyResourceTypeTo")
    def to: String = physicalResourceType

  opaque type HumanResourceId = String

  object HumanResourceId:
    def from(hId: String): Result[HumanResourceId] =
      val pattern = "HRS_.*".r
      if (pattern.matches(hId)) Right(hId) else Left(InvalidHumanId(hId))

  extension (humanId: HumanResourceId)
    @targetName("humanResourceTypeTo")
    def to: String = humanId
