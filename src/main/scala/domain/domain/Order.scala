package domain.domain

import domain.Result
import domain.domain.SimpleTypes.*
import domain.DomainError.*

import scala.xml.Node
import xml.XML.*

case class Order(orderId: OrderId, product: Product, quantity: Quantity)

object Order:
  def from(lp: List[Product])(xml: Node): Result[Order] =
    for
      id <- fromAttribute(xml, "id")
      orderId <- OrderId.from(id)
      prdref <- fromAttribute(xml, "prdref")
      prod <- ProductId.from(prdref)
      product <- lp.find(p => p.productId == prod).fold(Left(ProductDoesNotExist(prdref)))(p => Right(p))
      q <- fromAttribute(xml, "quantity")
      quantity <- Quantity.from(q)
    yield (Order(orderId, product, quantity))

