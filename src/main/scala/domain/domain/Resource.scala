package domain.domain

import domain.domain.SimpleTypes.*

trait Resource:
  
  def hasResourceType(physicalResourceType:PhysicalResourceType): Boolean

