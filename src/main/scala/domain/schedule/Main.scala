package domain.schedule

import io.FileIO
import domain.domain.Task
import domain.domain.Production
import domain.domain.Order
import domain.domain.TaskSchedule
import domain.domain.SimpleTypes
import domain.schedule.ScheduleMS01.*
import domain.Result
import scala.xml.{Elem, PrettyPrinter, XML, Utility}

object Main:
  def main(args: Array[String]) =
    val resultElem: Result[Elem] = for
      rootXML <- FileIO.load("/Users/madureiraa2/IdeaProjects/ISEP/TAP/tap-2021-m1a-1171150-1171162-1180104/files/functionalTests/ms03/functionalTest3_in.xml")
      scheduleXML <- ScheduleMS03.create(rootXML)
    yield scheduleXML

    resultElem.fold(de => println(de), result =>
      FileIO.save("/Users/madureiraa2/IdeaProjects/ISEP/TAP/tap-2021-m1a-1171150-1171162-1180104/files/functionalTests/ms03/functionalTest3_out.xml", result)
    )