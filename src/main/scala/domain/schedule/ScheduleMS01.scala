package domain.schedule

import scala.xml.{Elem, Node}
import domain.Result
import domain.domain.SimpleTypes.*
import domain.DomainError
import domain.DomainError.*
import domain.domain.{HumanResource, Order, PhysicalResource, Production, ScheduleDomain, Task, TaskSchedule, Resource}

object ScheduleMS01 extends Schedule :

  def create(xml: Elem): Result[Elem] =
    for
      production <- Production.from(xml)
      scheduleDomain <- extractProductionToScheduleDomain(production)
      resultXML <- ScheduleDomain.toXML(scheduleDomain)
    yield resultXML

  def extractProductionToScheduleDomain(production: Production): Result[ScheduleDomain] =
    for
      taskScheduleList <- extractProductionToTaskScheduleListByOrders(production)
    yield ScheduleDomain(taskScheduleList._1)

  private def extractProductionToTaskScheduleListByOrders(production: Production): Result[(List[TaskSchedule], TaskInstant)] =
    production.orders.foldLeft[Result[(List[TaskSchedule], TaskInstant)]](Right(Nil, TaskInstant.zero)) { case (accumTaskScheduleList, order) =>
      val quantityList:List[Int]= List.tabulate(order.quantity.to)(n => n + 1)
      for
        accumTaskScheduleList2 <- accumTaskScheduleList
        result <- extractProductionToTaskScheduleListByQuantity(production, order, quantityList, accumTaskScheduleList2._2) 
      yield (accumTaskScheduleList2._1 ::: result._1, result._2)
    }

  private def extractProductionToTaskScheduleListByQuantity(production: Production, order: Order, quantityList: List[Int],
                                                    currentTime: TaskInstant): Result[(List[TaskSchedule], TaskInstant)] =
    quantityList.foldLeft[Result[(List[TaskSchedule], TaskInstant)]](Right((Nil, currentTime))) { case (accumTaskScheduleList, prodNumber) =>
      for
        accumTaskScheduleList2 <- accumTaskScheduleList
        taskScheduleListAndTimeTuple <- extractTaskSchedulesFromOrder(order, production.humanResources, production.physicalResources, accumTaskScheduleList2._2, prodNumber)
      yield (accumTaskScheduleList2._1 ::: taskScheduleListAndTimeTuple._1, taskScheduleListAndTimeTuple._2)
    }

  private def extractTaskSchedulesFromOrder(order: Order, humanResourcesFromProduction: Seq[HumanResource], physicalResourcesFromProduction: Seq[PhysicalResource],
                                            currentTime: TaskInstant, productNumber: Int): Result[(List[TaskSchedule], TaskInstant)] =
    order.product.tasksList.foldLeft[Result[(List[TaskSchedule], TaskInstant)]](Right((Nil, currentTime))) { case (accumTuple, task) =>
      for
        accumTuple2 <- accumTuple
        humanResourceListResult <- getResourcesOfTask(humanResourcesFromProduction, task)
        phyResourceListResult <- getResourcesOfTask(physicalResourcesFromProduction, task)
        finalTime = accumTuple2._2 + task.duration
      yield (accumTuple2._1 :+ TaskSchedule(order, productNumber, task, accumTuple2._2, finalTime, phyResourceListResult, humanResourceListResult), finalTime)
    }


  private def getResourcesOfTask[R <: Resource](resourcesFromProduction: Seq[R], task: Task): Result[List[R]] =
    task.physicalResourceTypes.foldLeft[Result[List[R]]](Right(Nil)) { case (resourceList, phyResourceType) =>
      for
        resourceList1 <- resourceList
        resourceResult <- validateResourceAvailable(resourcesFromProduction, resourceList1, phyResourceType, task.id)
      yield (resourceList1 :+ resourceResult)
    }
  
  
  private def validateResourceAvailable[R <: Resource](resources: Seq[R], resourceList1: List[R],
                                             phyResourceType: PhysicalResourceType, taskId: TaskId): Result[R] =
    val optionResultPhyResource: Option[R] = resources
      .filter(resource => resource.hasResourceType(phyResourceType))
      .find(resource1 => !resourceList1.contains(resource1)) //de forma a evitar a repeticao de recursos na lista de recuros
    optionResultPhyResource.fold[Result[R]](Left(ResourceUnavailable(taskId.to, phyResourceType.to)))(Right(_))

  /*private def getHumanResourcesOfTask(humanResources: Seq[HumanResource], task: Task): Result[List[HumanResource]] =
    task.physicalResourceTypes.foldLeft[Result[List[HumanResource]]](Right(Nil)) { case (humanResourceList, phyResourceType) =>
      for
        humanResourceList1 <- humanResourceList
        humanResourceResult <- validateHumanResourceAvailable(humanResources, humanResourceList1, phyResourceType, task.id)
      yield (humanResourceList1 :+ humanResourceResult)
    }

  private def getPhysicalResourcesOfTask(phyResourcesFromProduction: Seq[PhysicalResource], task: Task): Result[List[PhysicalResource]] =
    task.physicalResourceTypes.foldLeft[Result[List[PhysicalResource]]](Right(Nil)) { case (physicalResourceList, phyResourceType) =>
      for
        physicalResourceList1 <- physicalResourceList
        physicalResourceResult <- validatePhysicalResourceAvailable(phyResourcesFromProduction, physicalResourceList1, phyResourceType, task.id)
      yield (physicalResourceList1 :+ physicalResourceResult)
    }

  private def validateHumanResourceAvailable(humanResources: Seq[HumanResource], humanResourceList1: List[HumanResource],
                                     phyResourceType: PhysicalResourceType, taskId: TaskId): Result[HumanResource] =
    val optionResultHumanResource: Option[HumanResource] = humanResources
      .filter(humanResource => humanResource.handles.find(handle => handle == phyResourceType).isDefined)
      .find(humanResource1 => !humanResourceList1.contains(humanResource1))
    optionResultHumanResource.fold[Result[HumanResource]](Left(ResourceUnavailable(taskId.to, phyResourceType.to)))(Right(_))

  private def validatePhysicalResourceAvailable(phyResources: Seq[PhysicalResource], physicalResourceList1: List[PhysicalResource],
                                        phyResourceType: PhysicalResourceType, taskId: TaskId): Result[PhysicalResource] =
    val optionResultPhyResource: Option[PhysicalResource] = phyResources
      .filter(phyResource => phyResource.phyResourceType == phyResourceType)
      .find(phyResource1 => !physicalResourceList1.contains(phyResource1))
    optionResultPhyResource.fold[Result[PhysicalResource]](Left(ResourceUnavailable(taskId.to, phyResourceType.to)))(Right(_))*/
