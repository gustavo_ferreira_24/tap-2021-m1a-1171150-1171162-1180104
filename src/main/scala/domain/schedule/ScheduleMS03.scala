package domain.schedule

import scala.xml.{Elem, Node}
import domain.Result
import domain.domain.SimpleTypes.*
import domain.DomainError
import domain.DomainError.*
import domain.domain.{HumanResource, Order, PhysicalResource, Production, ScheduleDomain, Task, TaskSchedule}

object ScheduleMS03 extends Schedule :

  def create(xml: Elem): Result[Elem] =
    for
      production <- Production.from(xml)
      scheduleDomain <- extractProductionToScheduleDomain(production)
      resultXML <- ScheduleDomain.toXML(scheduleDomain)
    yield resultXML

  def extractProductionToScheduleDomain(production: Production): Result[ScheduleDomain] =
    val mapOrderTasks: Map[Order, List[Task]] = extractMapOfTasksFromOrders(production.orders)
    for
      taskScheduleList <- computeScheduleDomain(production.physicalResources.toList, production.humanResources.toList, List(), List(), mapOrderTasks, TaskInstant.zero)
    yield ScheduleDomain(convertToFinalTaskScheduleList(taskScheduleList))

  private def convertToFinalTaskScheduleList(taskScheduleList: List[TaskSchedule]): List[TaskSchedule] =
    taskScheduleList.map(tl => {
      val orderIsSplited = tl.order.orderId.to.split("_")
      val newOrderId: OrderId = OrderId.from((orderIsSplited.head + "_" + orderIsSplited(1))).fold(_ => tl.order.orderId, newId => newId)
      val order: Order = Order(newOrderId, tl.order.product, tl.order.quantity)
      TaskSchedule(order, orderIsSplited.last.toInt, tl.task, tl.startingTime, tl.finalTime, tl.physicalResources, tl.humanResources)
    }).sortBy(ts => (ts.startingTime.to, ts.order.orderId.to))

  private def computeScheduleDomain(phyResources: List[PhysicalResource],
                                    humanResources: List[HumanResource],
                                    allocatedTasks: List[TaskSchedule],
                                    currentRunningTasks: List[TaskSchedule],
                                    tasksToAllocate: Map[Order, List[Task]],
                                    currentTime: TaskInstant): Result[List[TaskSchedule]] =
    if (tasksToAllocate.isEmpty && currentRunningTasks.isEmpty) then Right(allocatedTasks)
    else
      val firstTaskOfEachOrder: Map[Order, Task] = extractFirstTaskOfOrderMap(tasksToAllocate)
      val newTasksToAllocate: List[TaskSchedule] = allocateTasks(phyResources, humanResources, firstTaskOfEachOrder, currentRunningTasks, currentTime, allocatedTasks)
      val newFutureTasks: Map[Order, List[Task]] = filterFutureTasks(tasksToAllocate, newTasksToAllocate).filter(_._2.nonEmpty)
      if (currentRunningTasks.isEmpty && newTasksToAllocate.isEmpty && tasksToAllocate.nonEmpty) then Left(ImpossibleSchedule)
      else
        val newCurrentTasks: List[TaskSchedule] = List.concat(currentRunningTasks, newTasksToAllocate)
        val tasksThatFinishFirst: List[TaskSchedule] = getMostRecentTasks(newCurrentTasks)
        val newFinalTimeFromPast: TaskInstant = tasksThatFinishFirst.head.finalTime
        val newOldTasks: List[TaskSchedule] = allocatedTasks ::: tasksThatFinishFirst
        val newPresentTasks: List[TaskSchedule] = newCurrentTasks.filterNot(tasksThatFinishFirst.contains)
        computeScheduleDomain(phyResources, humanResources, newOldTasks, newPresentTasks, newFutureTasks, newFinalTimeFromPast)

  private def extractFirstTaskOfOrderMap(mapOrderTasks: Map[Order, List[Task]]): Map[Order, Task] =
    mapOrderTasks.foldLeft[Map[Order, Task]](Map()) { case (accum, mapElement) =>
      accum + (mapElement._1 -> mapElement._2.head)
    }

  private def allocateTasks(phyResources: List[PhysicalResource],
                            humanResources: List[HumanResource],
                            firstTaskOfEachOrder: Map[Order, Task],
                            currentRunningTasks: List[TaskSchedule],
                            currentTime: TaskInstant,
                            allocatedTasks: List[TaskSchedule]): List[TaskSchedule] =
    val filteredOrderTasks1 = filterTaskOrderMapByValidOrders(firstTaskOfEachOrder, currentRunningTasks)
    val possiblePhyResources = getPossiblePhysicalResources(phyResources, currentRunningTasks)
    val possibleHumanResources = getPossibleHumanResources(humanResources, currentRunningTasks)
    if (possiblePhyResources.isEmpty || possibleHumanResources.isEmpty) then List()
    else
      val filteredOrderTasks2 = filterTaskOrderMapByValidResources(filteredOrderTasks1, possiblePhyResources, possibleHumanResources)
      computeBestTasksForInstant(filteredOrderTasks2, possiblePhyResources, possibleHumanResources, currentRunningTasks, currentTime, allocatedTasks)

  private def computeBestTasksForInstant(filteredOrderTasks: Map[Order, Task],
                                         possiblePhyResources: List[PhysicalResource],
                                         possibleHumanResources: List[HumanResource],
                                         currentRunningTasks: List[TaskSchedule],
                                         currentTime: TaskInstant,
                                         allocatedTasks: List[TaskSchedule]): List[TaskSchedule] =
    val allPossibleCombinations: List[List[(Order, Task)]] = generatePossibleCombinationsOfOrderTaskMap(filteredOrderTasks, possiblePhyResources.length, possibleHumanResources.length)
    getMostEffectiveCombination1(allPossibleCombinations, possiblePhyResources, possibleHumanResources, currentTime, allocatedTasks)

  private def generatePossibleCombinationsOfOrderTaskMap(filteredOrderTasks: Map[Order, Task],
                                                         phyResourcesLength: Int,
                                                         humanResourcesLength: Int): List[List[(Order, Task)]] =
    val maxNumberCombinations = List(phyResourcesLength, humanResourcesLength, filteredOrderTasks.size, 5).min
    val orderTasksAsList = filteredOrderTasks.foldLeft[List[(Order, Task)]](List()) { case (accum, mapElement) => accum :+ (mapElement._1, mapElement._2) }

    List.tabulate(maxNumberCombinations)(n => n + 1).foldLeft[List[List[(Order, Task)]]](Nil) { case (accum, index) =>
      (accum ::: orderTasksAsList.combinations(index).flatMap(_.permutations).toList)
    }

  private def getMostEffectiveCombination1(combinations: List[List[(Order, Task)]],
                                           phyResources: List[PhysicalResource],
                                           humanResources: List[HumanResource],
                                           currentTime: TaskInstant,
                                           allocatedTasks: List[TaskSchedule]): List[TaskSchedule] =
    combinations.foldLeft[List[TaskSchedule]](Nil) { case (accum, combination) =>
      val combination1 = orderByProductNumber(combination)
      val newCombination = getMostEffectiveCombination2(combination1, phyResources, humanResources, currentTime)
      if (newCombination.length > accum.length)
        newCombination
      else if (newCombination.length < accum.length)
        accum
      else if (getCountOfOrdersInProductionByTask(newCombination, allocatedTasks) > getCountOfOrdersInProductionByTask(accum, allocatedTasks))
        newCombination
      else if (newCombination.map(_.order.orderId.getOrderIdWithoutQuantity.to).distinct.length < accum.map(_.order.orderId.getOrderIdWithoutQuantity.to).distinct.length)
        newCombination
      else if (newCombination.map(ts => getIndexOfOrder(ts.order)).sum < accum.map(ts => getIndexOfOrder(ts.order)).sum)
        newCombination
      else if (newCombination.map(ts => getProdNumberOfOrder(ts.order)).sum < accum.map(ts => getProdNumberOfOrder(ts.order)).sum)
        newCombination
      else
        accum
    }

  private def orderByProductNumber(orderTasks: List[(Order, Task)]): List[(Order, Task)] =
    // Map(Order Id sem quantity, List[orderTask]). Sorte de cada lista pelo prod number e concat
    orderTasks.foldLeft[Map[String, List[(Order, Task)]]](Map()) { case(accum, elem) =>
      val orderIdElem = elem._1.orderId.getOrderIdWithoutQuantity
      if (accum.get(orderIdElem).isDefined)
        (accum + (orderIdElem -> (accum.get(orderIdElem).get :+ elem)))
      else
        (accum + (orderIdElem -> List(elem)))
    }.foldLeft[List[(Order, Task)]](Nil) { case (accum, elem) =>
      (accum ::: elem._2.sortBy(orderTask => getProdNumberOfOrder(orderTask._1)))
    }




  private def getCountOfOrdersInProductionByTask(tasksToAllocate: List[TaskSchedule], allocatedTasks: List[TaskSchedule]): Int =
    val allocatedTasks2 = allocatedTasks.map(_.order)
    tasksToAllocate.count(ts => allocatedTasks2.contains(ts.order))

  private def getIndexOfOrder(order: Order): Int = order.orderId.to.split("_")(1).toInt

  private def getProdNumberOfOrder(order: Order): Int = order.orderId.to.split("_").last.toInt

  private def getMostEffectiveCombination2(singleCombination: List[(Order, Task)],
                                           phyResources: List[PhysicalResource],
                                           humanResources: List[HumanResource],
                                           currentTime: TaskInstant): List[TaskSchedule] =
    singleCombination.foldLeft[(Boolean, List[PhysicalResource], List[HumanResource], List[TaskSchedule])](true, phyResources, humanResources, Nil) {
      case (accum, orderTask) =>
        if (!accum._1) (accum._1, accum._2, accum._3, accum._4)
        else
          val physicalResources2: List[PhysicalResource] = getPhysicalResourcesOfTask(accum._2, orderTask._2).getOrElse(List())
          val humanResources2: List[HumanResource] = getHumanResourcesOfTask(accum._3, orderTask._2).getOrElse(List())
          if (physicalResources2.isEmpty || humanResources2.isEmpty)
            (false, List(), List(), List())
          else
            (true, accum._2.filterNot(physicalResources2.contains), accum._3.filterNot(humanResources2.contains),
              accum._4 :+ TaskSchedule(orderTask._1, 0, orderTask._2, currentTime, currentTime + orderTask._2.duration, physicalResources2, humanResources2))
    }._4


  // Create TaskSchedule objects from the combination of order and task
  private def createTaskSchedulesFromOrderTask(combination: List[(Order, Task, List[PhysicalResource], List[HumanResource])], currentTime: TaskInstant): List[TaskSchedule] =
    combination.map(comb => {
      val orderIsSplited = comb._1.orderId.to.split("_")
      val productNumber: Int = orderIsSplited.last.toInt
      val orderId = OrderId.from(orderIsSplited.head + "_" + orderIsSplited(1)).fold(_ => comb._1.orderId, newOrderId => newOrderId)
      val newOrder: Order = Order(orderId, comb._1.product, comb._1.quantity)
      val finalTime: TaskInstant = currentTime.+(comb._2.duration)
      TaskSchedule(newOrder, productNumber, comb._2, currentTime, finalTime, comb._3, comb._4)
    })

  // Get available physical resources
  private def getPossiblePhysicalResources(phyResources: List[PhysicalResource], currentRunningTasks: List[TaskSchedule]): List[PhysicalResource] =
    phyResources.filterNot(currentRunningTasks.flatMap(_.physicalResources).contains)

  // Get available human resources
  private def getPossibleHumanResources(humanResources: List[HumanResource], currentRunningTasks: List[TaskSchedule]): List[HumanResource] =
    humanResources.filterNot(currentRunningTasks.flatMap(_.humanResources).contains)

  // Return only the tasks that don't currently have a task from the same order being executed at the moment
  private def filterTaskOrderMapByValidOrders(mapOrderTask: Map[Order, Task],
                                              currentRunningTasks: List[TaskSchedule]): Map[Order, Task] =
    mapOrderTask.foldLeft[Map[Order, Task]](Map()) { case (newMap, orderTask) =>
      if (!currentRunningTasks.map(taskSchedule => taskSchedule.order.orderId).contains(orderTask._1.orderId))
      then newMap + orderTask
      else newMap
    }

  private def filterFutureTasks(tasksToAllocate: Map[Order, List[Task]], tasksAllocated: List[TaskSchedule]): Map[Order, List[Task]] =
    tasksToAllocate.foldLeft[Map[Order, List[Task]]](Map()) { case (accum, elem) =>
      if (tasksAllocated.map(_.order).contains(elem._1))
        (accum + (elem._1 -> elem._2.drop(1)))
      else
        (accum + elem)
    }

  // Return only the tasks that can be executed individually with the available resources
  private def filterTaskOrderMapByValidResources(mapOrderTask: Map[Order, Task],
                                                 possiblePhyResources: Seq[PhysicalResource],
                                                 possibleHumanResources: Seq[HumanResource]): Map[Order, Task] =
    mapOrderTask.foldLeft[Map[Order, Task]](Map()) { case (accum, orderTask) =>
      if (getPhysicalResourcesOfTask(possiblePhyResources, orderTask._2).isLeft || getHumanResourcesOfTask(possibleHumanResources, orderTask._2).isLeft)
      then accum
      else accum + orderTask
    }

  private def getPhysicalResourcesOfTask(phyResourcesFromProduction: Seq[PhysicalResource], task: Task): Result[List[PhysicalResource]] =
    task.physicalResourceTypes.foldLeft[Result[List[PhysicalResource]]](Right(Nil)) { case (physicalResourceList, phyResourceType) =>
      for
        physicalResourceList1 <- physicalResourceList
        physicalResourceResult <- validatePhysicalResourceAvailable(phyResourcesFromProduction, physicalResourceList1, phyResourceType, task.id)
      yield (physicalResourceList1 :+ physicalResourceResult)
    }

  private def getHumanResourcesOfTask(humanResources: Seq[HumanResource], task: Task): Result[List[HumanResource]] =
    task.physicalResourceTypes.foldLeft[Result[List[HumanResource]]](Right(Nil)) { case (humanResourceList, phyResourceType) =>
      for
        humanResourceList1 <- humanResourceList
        humanResourceResult <- validateHumanResourceAvailable(humanResources, humanResourceList1, phyResourceType, task.id)
      yield (humanResourceList1 :+ humanResourceResult)
    }

  private def validatePhysicalResourceAvailable(phyResources: Seq[PhysicalResource], physicalResourceList1: List[PhysicalResource],
                                                phyResourceType: PhysicalResourceType, taskId: TaskId): Result[PhysicalResource] =
    val optionResultPhyResource: Option[PhysicalResource] = phyResources
      .filter(phyResource => phyResource.phyResourceType == phyResourceType)
      .find(phyResource1 => !physicalResourceList1.contains(phyResource1))
    optionResultPhyResource.fold[Result[PhysicalResource]](Left(EmptyString))(Right(_))

  private def validateHumanResourceAvailable(humanResources: Seq[HumanResource], humanResourceList1: List[HumanResource],
                                             phyResourceType: PhysicalResourceType, taskId: TaskId): Result[HumanResource] =
    val optionResultHumanResource: Option[HumanResource] = humanResources
      .filter(humanResource => humanResource.handles.find(handle => handle == phyResourceType).isDefined)
      .find(humanResource1 => !humanResourceList1.contains(humanResource1))
    optionResultHumanResource.fold[Result[HumanResource]](Left(EmptyString))(Right(_))

  private def extractMapOfTasksFromOrders(orderList: Seq[Order]): Map[Order, List[Task]] =
    orderList.foldLeft[Map[Order, List[Task]]](Map()) { case (finalOrdersMap, order) =>
      extractMapOfTasksFromOrder(order).foldLeft[Map[Order, List[Task]]](finalOrdersMap) { case (finalMap, mapEntry) =>
        finalMap + mapEntry
      }
    }

  private def extractMapOfTasksFromOrder(order: Order): Map[Order, List[Task]] =
    val quantityList: List[Int] = List.tabulate(order.quantity.to)(n => n + 1)
    quantityList.foldLeft[Map[Order, List[Task]]](Map()) { case (newMap, quantityIncrement) =>
      newMap + (incrementOrderIdFromOrder(order, quantityIncrement) -> order.product.tasksList)
    }

  private def incrementOrderIdFromOrder(order: Order, index: Int): Order =
    val newOrderId: String = (order.orderId.to + "_" + index)
    val orderId: OrderId = OrderId.from(newOrderId).fold(_ => order.orderId, orderId => orderId)
    Order(orderId, order.product, order.quantity)

  // Returns the tasks that will finish first from those that are currently being executed
  private def getMostRecentTasks(tasks: List[TaskSchedule]): List[TaskSchedule] =
    val sortedTasksByTime: List[TaskSchedule] = tasks.sortBy(_.finalTime.to)
    sortedTasksByTime.filter(taskSchedule => taskSchedule.finalTime == sortedTasksByTime.head.finalTime)

  //metodo alternativo ao extractMapOfTasksFromOrders
  def orderToTaskSchedules(orders: Seq[Order]): Map[Order, List[TaskSchedule]] =
    orders.foldLeft[List[TaskSchedule]](Nil) {
      case (accumTaskScheduleList, order) =>
        val quantityList: List[Int] = List.tabulate(order.quantity.to)(n => n + 1)
        accumTaskScheduleList :::
          quantityList.foldLeft[List[TaskSchedule]](Nil) {
            case (accum, prodNumber) =>
              accum :::
                order.product.tasksList.foldLeft[List[TaskSchedule]](Nil) {
                  case (lts, task) =>
                    lts :+ TaskSchedule(order, prodNumber, task, TaskInstant.zero, TaskInstant.zero, List(), List())
                }

          }
    }.groupBy(_.order)
