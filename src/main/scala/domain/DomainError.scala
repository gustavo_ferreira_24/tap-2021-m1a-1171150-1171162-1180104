package domain

import scala.xml.Elem

type Result[A] = Either[DomainError, A]

object DomainError:
  def toXML(domainError: DomainError): Result[Elem] =
    Right(<ScheduleError xmlns="http://www.dei.isep.ipp.pt/tap-2021" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2021 ../../scheduleError.xsd">
      message={domainError.toString}
    </ScheduleError>)


enum DomainError:
  case IOFileProblem(error: String)
  case XMLError(error: String)
  case InvalidProductId(error: String)
  case InvalidOrderId(error: String)
  case ProductDoesNotExist(error: String)
  case InvalidQuantity(error: String)
  case TaskDoesNotExist(error: String)
  case EmptyString
  case InvalidPhysicalId(error: String)
  case InvalidTaskId(error: String)
  case InvalidTaskTime(error: String)
  case InvalidHumanId(error: String)
  case TaskUsesNonExistentPRT(error: String)
  case HumanUsesNonExistentPRT(error: String)
  case ResourceUnavailable(task: String, phyResourceType: String)
  case ImpossibleSchedule
